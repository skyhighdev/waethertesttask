//
//  ViewController.m
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "ViewController.h"

#import "CityTableCell.h"
#import "ContentManager.h"
#import "RequestManager.h"

#define kVerticalOffset 45.f
#define kTextViewExpandedHeightMultiplier 0.6666f
#define kTextViewCollapsedHeightMultiplier 0.3333f
#define kAnimationDuration 0.25f
@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak) IBOutlet UITableView* tableView;
@property (weak) IBOutlet UITextView* textView;
@property (weak) IBOutlet NSLayoutConstraint* heightConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@property (nonatomic, weak) NSArray<NSMutableDictionary*>* cities;
@property (assign) NSInteger selectedIndex;
@property (assign) BOOL textViewExpaneded;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.selectedIndex = -1;
    [self setTextViewExpanded:NO animated:NO];
    __weak typeof(self) weakSelf = self;
    [[RequestManager sharedManager] loginToWeatherCompletion:^(BOOL status) {
        
        [weakSelf layout];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Submethods

- (void)layout{
    
    self.cities = [ContentManager sharedManager].cities;
    [self.tableView reloadData];
}

- (void)getWeatherForCityID:(NSString*)cityID{
    __weak typeof(self) weakSelf = self;
    [self showloadIndicator];
    [[RequestManager sharedManager] getWeatherForCityByID:cityID completion:^(BOOL success, NSString *temperature, NSString *weather) {
        [weakSelf hideloadIndicator];
        [[ContentManager sharedManager] saveTemperature:temperature weather:weather forCityID:cityID];
        NSMutableDictionary* cityInfo = [[ContentManager sharedManager] cityInfoForCityID:cityID];
        [weakSelf setupTextViewWithCityInfo:cityInfo];
    }];
}

- (void)setupTextViewWithCityInfo:(NSMutableDictionary*)cityInfo{
    
    NSString* description = cityInfo[kDescription];
    NSString* temp = [NSString stringWithFormat:@"Temperature: %@ f",cityInfo[kTemperature]];
    
    NSString* weather = [NSString stringWithFormat:@"Weather: %@",cityInfo[kWeather]];
    NSString* text = [NSString stringWithFormat:@"%@\n\n%@\n\n%@",description,temp,weather];
    
    self.textView.text = text;
}

- (void)setTextViewExpanded:(BOOL)expanded animated:(BOOL)animated{
    
    float heightMultiplier = expanded ? kTextViewExpandedHeightMultiplier : kTextViewCollapsedHeightMultiplier;
    self.heightConstraint.constant = ([UIScreen mainScreen].bounds.size.height - kVerticalOffset)* heightMultiplier;
    __weak typeof(self) weakSelf = self;
    
    float duration = animated ? kAnimationDuration : 0.f;
    [UIView animateWithDuration:duration animations:^{
        
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        weakSelf.textViewExpaneded = expanded;
        [weakSelf scrollToSelectedCell];
    }];
}

- (void)showloadIndicator{
    self.indicatorView.hidden = NO;
    [self.indicatorView startAnimating];
}

- (void)hideloadIndicator{
    
    [self.indicatorView stopAnimating];
}

- (void)scrollToSelectedCell{
    
    __block BOOL selectedCellVisible = NO;
    NSArray* visiblePaths = self.tableView.indexPathsForVisibleRows;
    [visiblePaths enumerateObjectsUsingBlock:^(NSIndexPath*  _Nonnull indexPath, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (indexPath.row == self.selectedIndex) {
            selectedCellVisible = YES;
        }
    }];
    if (!selectedCellVisible && self.selectedIndex > 0) {
        NSIndexPath* selectedIndexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
        [self.tableView scrollToRowAtIndexPath:selectedIndexPath atScrollPosition:(UITableViewScrollPositionBottom) animated:YES];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* cellID = @"CityTableCell";
    CityTableCell* cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    NSMutableDictionary* cityInfo = self.cities[indexPath.row];
    [cell setCity:cityInfo[kCity] country:cityInfo[kCountry]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.selectedIndex == indexPath.row) {
        
        [self setTextViewExpanded:!self.textViewExpaneded animated:YES];
    } else{
        
        self.selectedIndex = indexPath.row;
        NSMutableDictionary* cityInfo = self.cities[indexPath.row];
        [self setupTextViewWithCityInfo:cityInfo];
        [self setTextViewExpanded:YES animated:YES];
        [self getWeatherForCityID:cityInfo[kID]];
    }
}


@end
