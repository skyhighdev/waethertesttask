//
//  CityTableCell.m
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "CityTableCell.h"

@interface CityTableCell()

@property (weak) IBOutlet UILabel* cityLabel;
@property (weak) IBOutlet UILabel* countryLabel;

@end

@implementation CityTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCity:(NSString*)city country:(NSString*)country{
    self.countryLabel.text = country;
    self.cityLabel.text = city;
}

@end
