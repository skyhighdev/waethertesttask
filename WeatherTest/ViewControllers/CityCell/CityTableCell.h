//
//  CityTableCell.h
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityTableCell : UITableViewCell

- (void)setCity:(NSString*)city country:(NSString*)country;

@end
