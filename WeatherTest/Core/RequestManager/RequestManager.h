//
//  RequestManager.h
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestManager : NSObject

+ (instancetype)sharedManager;

- (void)loginToWeatherCompletion:(void(^)(BOOL status))completion;
- (void)getWeatherForCityByID:(NSString*)cityID completion:(void(^)(BOOL success, NSString* temperature, NSString* weather))completion;

@end
