//
//  RequestManager.m
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "RequestManager.h"

#import "SafeCategories.h"

#define kBaseURL @"https://api.openweathermap.org/data/2.5/"
#define kWeatherByCityID(cityID) [NSString stringWithFormat:@"weather?id=%@&APPID=4f7a4e7d586fd96256652767cd112c5f",cityID]
                                                            //api.openweathermap.org/data/2.5/weather?id=2172797
#define kLoginToWeatherService @"forecast?id=524901&APPID=4f7a4e7d586fd96256652767cd112c5f"

@interface RequestManager ()

@end

@implementation RequestManager

+ (instancetype)sharedManager{
    
    static RequestManager* manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [RequestManager new];
    });
    return manager;
}

#pragma mark - Public

- (void)loginToWeatherCompletion:(void(^)(BOOL success))completion{
    
    NSString* url = kLoginToWeatherService;
    
    [self perform:@"GET" url:url completionHandler:^(id responseObject, NSHTTPURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (completion) {
            BOOL status = [self requestIsSuccessWithResponse:response];
            completion(status);
        }
    }];
}

- (void)getWeatherForCityByID:(NSString*)cityID completion:(void(^)(BOOL success, NSString* temperature, NSString* weather))completion{
    
    if (cityID.length <= 0) {
        if (completion)
            completion(NO, nil, nil);
        return;
    }
    
    NSString* url = kWeatherByCityID(cityID);
    
    [self perform:@"GET" url:url completionHandler:^(id responseObject, NSHTTPURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSArray* weatherObjects = [responseObject safeArrayObjectForKey:@"weather"];
        NSDictionary* weatherInfo = weatherObjects.count > 0 ? weatherObjects.firstObject : @{};
        NSString* weather = [weatherInfo safeStringObjectForKey:@"description"];
        
        NSDictionary* temperatureInfo = [responseObject safeDictionaryObjectForKey:@"main"];
        NSString* temperature = [temperatureInfo safeStringObjectForKey:@"temp"];
        
        if (completion) {
            BOOL status = [self requestIsSuccessWithResponse:response];
            completion(status, temperature, weather);
        }
    }];
}

#pragma mark - Private

- (void)perform:(NSString*)httpMethod url:(NSString*)urlString completionHandler:(void (^)(id responseObject, NSHTTPURLResponse * _Nullable response, NSError * _Nullable error))completionHandler{
    
    NSString* fullURLString = [NSString stringWithFormat:@"%@%@",kBaseURL,urlString];
    
    NSURL* url = [NSURL URLWithString:fullURLString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    request.HTTPMethod = httpMethod;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionDataTask* task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError* jsonError = nil;
        id jsonObject = data ? [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&jsonError] : nil;
        
        if (completionHandler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(jsonObject, (NSHTTPURLResponse*)response, error);
            });
        }
    }];
    [task resume];
    
}

#pragma mark - Request Helpers

- (BOOL)requestIsSuccessWithResponse:(NSHTTPURLResponse*)response{
    
    BOOL status = response.statusCode == 200 || response.statusCode == 201 ? YES : NO;
    return status;
}


@end
