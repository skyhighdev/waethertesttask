//
//  ContentManager.h
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kID @"ID"
#define kCity @"city"
#define kCountry @"country"
#define kDescription @"description"
#define kTemperature @"temperature"
#define kWeather @"weather"


@interface ContentManager : NSObject

+ (instancetype)sharedManager;

- (NSMutableDictionary*)cityInfoForCityID:(NSString*)cityID;
- (void)saveTemperature:(NSString*)temperature weather:(NSString*)weather forCityID:(NSString*)cityID;
    
@property (nonatomic, strong, readonly) NSArray<NSMutableDictionary*>* cities;


@end
