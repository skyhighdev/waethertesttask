//
//  ContentManager.m
//  WeatherTest
//
//  Created by Developer on 9/26/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "ContentManager.h"

#define kStoredContentKey @"StoredContentKey"

@interface ContentManager()

@property (nonatomic, strong) NSArray<NSMutableDictionary*>* cities;

@end

@implementation ContentManager

#pragma mark - Override

- (instancetype)init{
    
    self = [super init];
    if (self) {
        self.cities = [self retrieveStoredInfo];
    }
    return self;
}

#pragma mark - Public

+ (instancetype)sharedManager{
    
    static ContentManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [ContentManager new];
    });
    return manager;
}

- (void)saveTemperature:(NSString*)temperature weather:(NSString*)weather forCityID:(NSString*)cityID{
    
    NSMutableDictionary* cityInfo = [self cityInfoForCityID:cityID];
    
    if (weather.length > 0)
        [cityInfo setObject:weather forKey:kWeather];
    if (temperature.length > 0)
        [cityInfo setObject:temperature forKey:kTemperature];

    [self storeCitiesInfo:self.cities];
}

- (NSMutableDictionary*)cityInfoForCityID:(NSString*)cityID{
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"self.ID like[c] %@",cityID];
    NSArray<NSMutableDictionary*>* filteredArray = [self.cities filteredArrayUsingPredicate:predicate];
    NSUInteger index = [self.cities indexOfObject:filteredArray.firstObject];
    NSMutableDictionary* cityInfo = [self.cities objectAtIndex:index];
    return cityInfo;
}

- (void)storeCitiesInfo{
    
    [self storeCitiesInfo:self.cities];
}

#pragma mark - Private

- (void)storeCitiesInfo:(NSArray<NSMutableDictionary*>*)info{
    
    [[NSUserDefaults standardUserDefaults] setObject:info forKey:kStoredContentKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray<NSMutableDictionary*>*)retrieveStoredInfo{
    
    NSArray<NSMutableDictionary*>* storedInfo = nil;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kStoredContentKey]) {
        NSArray<NSDictionary*>* immutableInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kStoredContentKey];
        
        NSMutableArray* mutableInfo = [NSMutableArray new];
        [immutableInfo enumerateObjectsUsingBlock:^(NSDictionary*  _Nonnull cityInfo, NSUInteger idx, BOOL * _Nonnull stop) {
            
            [mutableInfo addObject:[cityInfo mutableCopy]];
        }];
        storedInfo = [NSArray arrayWithArray:mutableInfo];
    } else{
        storedInfo = [self getDefaultDictionary];
    }
    return storedInfo;
}

#pragma mark -

- (NSArray<NSMutableDictionary*>* )getDefaultDictionary{
    
    NSMutableDictionary* item1 = [@{kID : @"4317656",
                            kCity : @"Boston",
                            kCountry : @"US",
                            kDescription : @"Fugiat nulla pariatur voluptate velit esse cillum. Voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item2 = [@{kID : @"2147714",
                            kCity : @"Sydney",
                            kCountry : @"AU",
                            kDescription : @"Lcillum dolore eu fugiat nulla pariatur voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item3 = [@{kID : @"2643743",
                            kCity : @"London",
                            kCountry : @"GB",
                            kDescription : @"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item4 = [@{kID : @"703447",
                            kCity : @"Kyiv",
                            kCountry : @"UA",
                            kDescription : @"Voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item5 = [@{kID : @"1850147",
                            kCity : @"Tokyo",
                            kCountry : @"JP",
                            kDescription : @"The Greater Tokyo Area is the most populous metropolitan area in the world.[8] It is the seat of the Emperor of Japan and the Japanese government. ",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item6 = [@{kID : @"3169070",
                            kCity : @"Rome",
                            kCountry : @"IT",
                            kDescription : @"Rome also serves as the capital of the Lazio region. With 2,877,215 residents in 1,285 km2 (496.1 sq mi), it is also the country's most populated comune.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSMutableDictionary* item7 = [@{kID : @"3996063",
                            kCity : @"Mexico",
                            kCountry : @"MX",
                            kDescription : @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                            kTemperature : @"",
                            kWeather : @""
                            } mutableCopy];
    
    NSArray<NSMutableDictionary*>* result = @[ item1, item2, item3, item4, item5, item6, item7 ];
    return result;
}




@end
